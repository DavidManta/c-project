﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectPaw
{
    class Operatii : ICloneable
    {
        private List<Materiale> listaMateriale;
        public string denumire;

        public string Denumire
        {
            get { return denumire; }
            set { denumire = value; }
        }

        public List<Materiale> ListaMateriale
        {
            get { return listaMateriale; }
            set { listaMateriale = value; }
        }

        public Operatii()
        {
            denumire = "";
            listaMateriale = new List<Materiale>();
        }

        public override string ToString()
        {
            string rezultat = "";
            rezultat += "Operatii cu denumirea " + denumire + " are urmatoarele materiale: ";
            foreach (Materiale a in listaMateriale)
                rezultat += a.ToString() + Environment.NewLine;
            return rezultat;
        }



        object ICloneable.Clone()
        {
            return this.MemberwiseClone();
        }
    }
}

