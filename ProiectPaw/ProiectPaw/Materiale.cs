﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProiectPaw
{
    class Materiale : ICloneable, IComparable
    {
        private String nume;
        private int cantitate;
        private int[] marimi;


        public Materiale()
        {
            nume = "";
            cantitate = 0;
            marimi = null;
        }
        public Materiale(String nume, int cantitate, int[] marimi)
        {
            this.nume = nume;
            this.cantitate = cantitate;
            this.marimi = new int[marimi.Length];
            for (int i = 0; i < marimi.Length; i++)
                this.marimi[i] = marimi[i];
        }

        public override string ToString()
        {
            String rezultat = "Materialul " + this.nume + " cu cantitatea " + this.cantitate + " si marimile : ";
            for (int i = 0; i < marimi.Length; i++)
            {
                rezultat += marimi[i] + " ";

            }
            return rezultat;
        }

        object ICloneable.Clone()
        {
            return this.MemberwiseClone();
        }

        public Materiale Clone()
        {
            Materiale s = (Materiale)((ICloneable)this).Clone();
            int[] marimiNoi = (int[])marimi.Clone();
            s.marimi = marimiNoi;
            return s;
        }

        int IComparable.CompareTo(object obj)
        {
            Materiale s = (Materiale)obj;

            if (this.cantitate < s.cantitate)
                return -1;
            else
                if (this.cantitate > s.cantitate)
                    return 1;
                else
                    return string.Compare(this.nume, s.nume);
        }


        public static Materiale operator +(Materiale s, int marime)
        {
            int[] marimiNoi = new int[s.marimi.Length + 1];
            for (int i = 0; i < s.marimi.Length; i++)
                marimiNoi[i] = s.marimi[i];
            marimiNoi[marimiNoi.Length - 1] = marime;
            s.marimi = marimiNoi;
            return s;
        }

        public int this[int index]
        {
            get
            {
                if (index >= 0 && index < this.marimi.Length)
                    return this.marimi[index];
                else
                    return -1;
            }
        }


    }
}