﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace ProiectPaw
{
    public partial class Form1 : Form
    {
        System.Collections.ArrayList lista = new ArrayList();
        public OleDbConnection connection = new OleDbConnection();
       
        public Form1()
        {
            InitializeComponent();
            connection.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\Users\Remus\Desktop\materiale1.accdb;
Persist Security Info=False;";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
               
                
                string nume = tb_nume.Text; 
                int cantitate = Convert.ToInt32(tb_cantitate.Text);
                string[] marimiS = tb_marimi.Text.Split(','); 
                int[] marimi = new int[marimiS.Length];
                for (int i = 0; i < marimiS.Length; i++)
                    marimi[i] = Convert.ToInt32(marimiS[i]);
                MessageBox.Show("Materialul a fost adaugat!");
                Materiale s = new Materiale(nume, cantitate, marimi); 
                lista.Add(s);
                tb_cantitate.Clear();
                tb_marimi.Clear();
                tb_nume.Clear();
                


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                connection.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;
                command.CommandText = "select * from table1";
                
               OleDbDataReader reader= command.ExecuteReader();
                while(reader.Read())
                {
                    comboBox1.Items.Add(reader["Nume"]);
                }
               connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("error " + ex);
            }
            try
            {
                connection.Open();
                CheckConnection.Text = "Conectat";
                connection.Close();
            }catch(Exception ex)
            {
                MessageBox.Show("error" + ex);
            }

        }

        private void rosuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.tb_nume.BackColor = System.Drawing.Color.Red;
            this.tb_cantitate.BackColor = System.Drawing.Color.Red;
            this.tb_marimi.BackColor = System.Drawing.Color.Red;
        }

        private void verdeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.tb_nume.BackColor = System.Drawing.Color.Green;
            this.tb_cantitate.BackColor = System.Drawing.Color.Green;
            this.tb_marimi.BackColor = System.Drawing.Color.Green;
        }

        private void galbenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.tb_nume.BackColor = System.Drawing.Color.Yellow;
            this.tb_cantitate.BackColor = System.Drawing.Color.Yellow;
            this.tb_marimi.BackColor = System.Drawing.Color.Yellow;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 frm = new Form2(lista);
            frm.ShowDialog();
        }

        private void tb_cantitate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9' || e.KeyChar == (char)8 || e.KeyChar == '.')
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void tb_cantitate_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form3 frm = new Form3();
            frm.ShowDialog();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                connection.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;
                command.CommandText = "insert into table1 (Nume,Cantitate,Marimi) values('" + tb_nume.Text + "','" + tb_cantitate.Text + "','" + tb_marimi.Text + "')";
                command.ExecuteNonQuery();
                MessageBox.Show("Salvat!");
            }catch(Exception ex)
            {
                MessageBox.Show("error " + ex);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
             try
            {
                connection.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;
                command.CommandText = "insert into table1 (Nume,Cantitate,Marimi) values('" + tb_nume.Text + "','" + tb_cantitate.Text + "','" + tb_marimi.Text + "')";
                command.ExecuteNonQuery();
                MessageBox.Show("Salvat!");
            }catch(Exception ex)
            {
                MessageBox.Show("error " + ex);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                connection.Open();
                OleDbCommand command = new OleDbCommand();
                command.Connection = connection;
                command.CommandText = "delete from table1 where Nume='"+tb_nume.Text+"' ";
                command.ExecuteNonQuery();
                MessageBox.Show("Sters!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("error " + ex);
            }
        }






       
    }
}