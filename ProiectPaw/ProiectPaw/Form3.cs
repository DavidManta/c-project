﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ProiectPaw
{
    public partial class Form3 : Form
    {
        private System.Collections.ArrayList lista;

        public Form3()
        {
            InitializeComponent();
        }

        public Form3(System.Collections.ArrayList lista)
        {
            // TODO: Complete member initialization
            this.lista = lista;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //preluam valorile din text box
            int i1 = int.Parse(textBox12.Text);
            int i2 = int.Parse(textBox11.Text);
            int i3 = int.Parse(textBox10.Text);
            int i4 = int.Parse(textBox9.Text);
            //convertim valorile in grade pentru a putea desena pie chart-ul
            float total = i1 + i2 + i3 + i4;
            float deg1 = (i1 / total) * 360;
            float deg2 = (i2 / total) * 360;
            float deg3 = (i3 / total) * 360;
            float deg4 = (i4 / total) * 360;
            //conturul
            Pen p = new Pen(Color.Black, 2);
            Graphics g = this.CreateGraphics();
            Rectangle rec = new Rectangle(textBox12.Location.X + textBox12.Size.Width + 10, 12, 150, 150);
            //culorile
            Brush b1 = new SolidBrush(Color.Red);
            Brush b2 = new SolidBrush(Color.Yellow);
            Brush b3 = new SolidBrush(Color.Black);
            Brush b4 = new SolidBrush(Color.Brown);
            //desen
            g.Clear(Form1.DefaultBackColor);
            g.DrawPie(p, rec, 0, deg1);
            g.FillPie(b1, rec, 0, deg1);
            g.DrawPie(p, rec, deg1, deg2);
            g.FillPie(b2, rec, deg1, deg2);
            g.DrawPie(p, rec, deg2 + deg1, deg3);
            g.FillPie(b3, rec, deg2 + deg1, deg3);
            g.DrawPie(p, rec, deg3 + deg2 + deg1, deg4);
            g.FillPie(b4, rec, deg3 + deg2 + deg1, deg4);
        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

            using (StreamReader reader = File.OpenText(@"G:\PAW\ProiectPaw\pred.txt"))
                    {
                        textBox12.Text = reader.ReadLine();
                        textBox11.Text = reader.ReadLine();
                        textBox10.Text = reader.ReadLine();
                        textBox9.Text = reader.ReadLine();
                    }
                }
            }

        }
    

