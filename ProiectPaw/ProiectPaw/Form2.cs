﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace ProiectPaw
{
    public partial class Form2 : Form
    {
        ArrayList lista2 = new ArrayList();
        public Form2(ArrayList lista)
        {
            InitializeComponent();
            lista2 = lista;
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (Materiale m in lista2)
                textBox1.Text += m.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        public string tb_cantitate_TextChanged { get; set; }

        private void button3_Click_1(object sender, EventArgs e)
        {
             
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
            listBox1.Items.Add(textBox1.Text);
            
            textBox1.Clear();
            MessageBox.Show("Banii au fost retrasi din cont.");
            
        }

        private void textBox1_MouseDown(object sender, MouseEventArgs e)
        {
            textBox1.SelectAll();
            listBox1.DoDragDrop(textBox1.Text, DragDropEffects.All);
        }

        private void listBox1_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
            listBox1.Items.Add(e.Data.GetData(DataFormats.Text));
            textBox1.Clear();
        }

        private void previewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.ShowDialog();
        }

        private void pd_print(object sender, PrintPageEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            e.Graphics.DrawString(textBox1.Text, Font, Brushes.Black, 200, 200);
        }

       

      
        
    }
}
